#!/usr/bin/env python3
""" Returning the location of the ISS in latitude/longitude"""
import requests
URL= "http://api.open-notify.org/iss-now.json" 
def main():
    resp= requests.get(URL).json()
    lon= resp["iss_position"]["latitude"]
    lat= resp["iss_position"]["longitude"]
    time= resp["timestamp"]
    print ("CURRENT LOCATION OF THE ISS:")
    print(f"""
    Lon: {lon}
    Lat: {lat}
    Time: {time}
    """)
if __name__=="__main__":
    main()
